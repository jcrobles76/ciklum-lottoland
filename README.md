# Instrucctions & Requirements

1.- In order to works with this project you needs Nodejs v12 in advance and npm v6 in advance
2.- Inside of path folder exectute command 'npm i --save' in order to download all dependecies and creates node_modules folder
3.- Once npm_modules folder was generated then you can run 'npm run dev' in order to run the project
4.- Development commits can be found in 'challenge' git branch.

# SOLVING CORS PROBLEM
NOTE: The API url "https://www.lottoland.com/en/eurojackpot/results-winning-numbers" provide in documentation is protected by CORS and is not accesible returning a "403 Forbidden Access". In order to solve this problem and the AJAX Request can works correctly I have used a middle proxy server in my requests to the API as solution to skip the cors problem and get the responses. This a provisional solution called cors-anywhere provide for a third that give us the possibility of skip cors restricctions. It's a free use tool that you can use for a limited period of time and previously to use the my Lottolands' app you needs request a temporary authorization pressing in the button you can find in the site http://cors-anywhere.herokuapp.com/corsdemo


# FRAMEWORK AND JAVASCRIPT USED ARE:
# Vue 3 + Typescript + Vite

This template should help get you started developing with Vue 3 and Typescript in Vite. The template uses Vue 3 `<script setup>` SFCs, check out the [script setup docs](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup) to learn more.

## Recommended IDE Setup

- [VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)

## Type Support For `.vue` Imports in TS

Since TypeScript cannot handle type information for `.vue` imports, they are shimmed to be a generic Vue component type by default. In most cases this is fine if you don't really care about component prop types outside of templates. However, if you wish to get actual prop types in `.vue` imports (for example to get props validation when using manual `h(...)` calls), you can enable Volar's `.vue` type support plugin by running `Volar: Switch TS Plugin on/off` from VSCode command palette.
